export class PhoneMessage {

    public id: Number;
    public message: string;
    public project_id: Number;
    public stance_id: Number;

    public static asignProps(t1: PhoneMessage, t2: PhoneMessage) {
        t1.id = t2.id;
        t1.message = t2.message;
        t1.project_id = t2.project_id;
        t1.stance_id = t2.stance_id;
    }

    public static getNew() {
        return new PhoneMessage();
    }

    public constructor(stance_id?: Number) {
        this.stance_id = stance_id;
    }

    public asignProps(t: PhoneMessage) {
        PhoneMessage.asignProps(this, t);
    }

}
